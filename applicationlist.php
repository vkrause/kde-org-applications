<?php

require('config.php');
require('includes/AppDataBuilder.php');

$url_split = explode('/', $_SERVER['REQUEST_URI']);
$url_dir = $url_split[1];

$index = json_decode(file_get_contents("index.json"), true);

$categories = array_keys($index);
$category = false;
foreach($categories as $cat) {
    if (strtolower($cat) == $_GET["category"]) {
        $category = $cat;
        break;
    }
}
if($category === false) {
    include("404.php");
    return;
}

require(KDE_ORG . '/aether/config.php');
$page_title=$category;
$pageConfig = array_merge($pageConfig, [
    'title' => $page_title,
    'cssFile' => 'https://cdn.kde.org/aether-devel/applications.css',
    'image' => '/applications/apps.png',
]);
require(KDE_ORG . '/aether/header.php');
$site_root = "../";

echo '<main class="container">';

echo '<h1><a href="/'.$url_dir.'/">KDE\'s Applications</a> '.$category.'</h1>';

$appDataBuilder = new AppDataBuilder();

$array = $appDataBuilder->loadCategory($index, $category);

echo '<h2>Applications</h2>';
echo '<div class="row">';

foreach($array['apps'] as $app) {
    $appDataBuilder->displayAppCard($app, $url_dir, $category);
}

echo '</div>';

if (sizeof($array['addons']) > 0) {
    echo '<h2>Addons</h2>';
    echo '<div class="row">';
}

foreach($array['addons'] as $addon) {
    $appDataBuilder->displayAppCard($addon, $url_dir, $category);
}

if (sizeof($array['addons']) > 0) {
    echo '</div>';
}

echo '<p>&nbsp;</p>';
echo '</main>';
require(KDE_ORG . '/aether/footer.php');
