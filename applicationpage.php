<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

include 'includes/AppDataBuilder.php';

$url_split = explode('/', $_SERVER['REQUEST_URI']);
$url_dir = $url_split[1];

// khelpcenter has an appstream id of org.kde.Help so the forward from the old URLs does not work
// this json file has that mapping
$contents = file_get_contents('unusual-appstream-ids.json');
$contents = utf8_encode($contents);
$unusual_appstream_ids = json_decode($contents, true);

// forward for old kde.org/applications site URLs
if (strpos($_GET['application'], 'org.kde') === false) {
    $application_name = $_GET['application'];
    if (array_key_exists($application_name, $unusual_appstream_ids)) {
        $application_name = $unusual_appstream_ids[$application_name];
    }
    header("Location: /".  $url_dir . "/" . $_GET['category'] . '/' . 'org.kde.' . $application_name);
    exit();
}

$appDataBuilder = new AppDataBuilder();
// if it doesn't exist return 404
$app = $appDataBuilder->load($_GET['application']);

if ($app === null) {
    http_response_code(404);
    print 'App not found';
    exit();
}

$category = $_GET['category'];
$development = isset($_GET['development']);

$index = json_decode(file_get_contents("index.json"), true);

$categories = array_keys($index);
foreach($categories as $cat) {
    if (strtolower($cat) == $_GET["category"]) {
        $category_string = $cat;
        break;
    }
}
if (!isset($category_string)) {
    http_response_code(404);
    print 'Category not found';
    exit();
}


// if app category is wrong forward to correct one
$correct_category = null;
foreach ($categories as $cat) {
    if (in_array($_GET['application'], $index[$cat])) {
        $correct_category = $cat;
    }
}
if (strtolower($correct_category) != $_GET['category']) {
    header("Location: /". $url_dir . '/' . strtolower($correct_category) . '/' . $_GET['application']);
    exit();
}

$page_title = $app->name();
if (!empty($app->genericName())) {
    $page_title = $page_title." - ".$app->genericName();
}

if ($development) {
    $page_title=$app->name()." - Development Information";
}

function printSidebar($app, $category, $development)
{
    $content = '';
    $url_split = explode('/', $_SERVER['REQUEST_URI']);
    $url_dir = $url_split[1];

    if (($app->hasHomepage() && !strpos($app->homepage(), 'kde.org/applications') > 0) || $app->hasKDEApps()) {
        $content .= '<div class="infobox"><strong>More about '.$app->name().'</strong><br />';
        if ($app->hasHomepage()) {
            $content .= '<a href="'.$app->homepage().'">'.$app->name().' Homepage</a>';
        }
        if ($app->hasKDEApps()) {
            $url = htmlspecialchars("http://kde-apps.org/content/show.php?content=".$app->KDEAppsId());
            $content .= '<a href="'.$url.'">'.$app->name().' on KDE-Apps.org</a>';
        }
        $content .= '</div>';
    }

    $content .= '<div><strong>Get help</strong>';
    if ($app->hasUserbase()) {
        $content .= '<br /><span itemprop="softwareHelp" itemscope itemtype="http://schema.org/CreativeWork">
            <a itemprop="url" href="'.$app->userbase().'">'.$app->name().' on UserBase</a>
        </span>';
    }
    $content .= '<br /><a href="'.$app->forumUrl().'">KDE Community Forums</a>';
    if ($app->hasHandbook()) {
        $content .= '<br /><span itemprop="softwareHelp" itemscope itemtype="https://schema.org/CreativeWork">
            <a itemprop="url" href="'.$app->handbook().'">'.$app->name().' Handbook</a>
        </span>';
    }
    $content .= '</div>';

    $content .= '<div class="infobox"><strong>Contact the authors</strong>';

    //Bug tracker links
    if ($app->hasBugTracker()) {
        if ($app->isBugTrackerExternal()) {
            $content .= '<br /><a href="'.htmlentities($app->bugzillaProduct()).'">Report a bug</a>';
        } else { //KDE Bugzilla
            $componentstring = "";
            if ($app->bugzillaComponent()) {
                $componentstring = '&amp;component='.$app->bugzillaComponent();
            }
            $content .= '<br /><a href="https://bugs.kde.org/enter_bug.cgi?format=guided&amp;product='.$app->bugzillaProduct().$componentstring.'">Report a bug</a>';
        }
    } else { //Empty bugtracker, use default link
        $content .= '<br /><a href="https://bugs.kde.org/wizard.cgi">Report a bug</a>';
    }
    foreach ($app->ircChannels() as $channel) {
        $content .= '<br />IRC: <a href="irc://irc.freenode.org/'.$channel.'">'.$channel.' on Freenode</a>';
    }
    foreach ($app->mailingLists() as $ml) {
        //KDE mailing lists
        if (substr($ml, -8, 8) == "@kde.org") {
            $base = substr($ml, 0, -8);
            $content .= '<br />Mailing List: <a href="mailto:'.htmlspecialchars($ml).'">' .htmlspecialchars($base).'</a>';
            $content .= '<br />(<a href="https://mail.kde.org/mailman/listinfo/'.$base.'">subscribe</a>, <a href="https://mail.kde.org/mailman/listinfo/'.$base.'/">list information</a>)';
        } else if (substr($ml, -22, 22) == "@lists.sourceforge.net") { //Sourceforge.net
            $base = substr($ml, 0, -22);
            $content .= '<br />Mailing List: <a href="mailto:'.htmlspecialchars($ml).'">'.htmlspecialchars($base).'</a>';
            $content .= '<br />(<a href="https://lists.sourceforge.net/lists/listinfo/'.$base.'">subscribe</a>, <a href="http://sourceforge.net/mailarchive/forum.php?forum_name='.$base.'">archive</a>)';
        } else if (substr($ml, 0, 31) == "http://groups.google.com/group/") { //Google Groups (web)
            $base = substr($ml, 31, strlen($ml)-31);
            $content .= '<br />Mailing List: <a href="mailto:'.htmlspecialchars($base).'@googlegroups.com">'.htmlspecialchars($base).'</a>';
            $content .= '<br />(<a href="http://groups.google.com/group/'.$base.'/subscribe?note=1">subscribe</a>, <a href="http://groups.google.com/group/'.$base.'/topics">archive</a>)';
        } else if (substr($ml, -17, 17) == "@googlegroups.com") { //Google Groups (mail)
            $base = substr($ml, 0, -17);
            $content .= '<br />Mailing List: <a href="mailto:'.htmlspecialchars($ml).'">'.htmlspecialchars($base).'</a>';
            $content .= '<br />(<a href="http://groups.google.com/group/'.$base.'/subscribe?note=1">subscribe</a>, <a href="http://groups.google.com/group/'.$base.'/topics">archive</a>)';
        } else { //Default mail
            $content .= '<br />Mailing List: <a href="mailto:'.htmlspecialchars($ml).'">'.$app->name().'</a>';
        }
    }
    $content .= '</div>';

    if (!$development) {
        $content .= '<div class="infobox"><strong><a href="'.$_SERVER['REQUEST_URI'].'/development">Development Information</a></strong></div>';
    }

    return $content;
}

function printPage($app, $category)
{
    $url_split = explode('/', $_SERVER['REQUEST_URI']);
    $url_dir = $url_split[1];

    if ($category == 'unmaintained') {
        print "<p><b>This app is unmaintained and no longer released by the KDE community.</b></p>";
    }

    print '<div itemprop="description">';
    print $app->descriptionHtml();
    print '</div>';
    if ($app->hasVersions() || $app->hasAuthors() || $app->hasLicense()) {
        if ($app->hasAuthors()) {
            print '<h2>Developed By</h2>';
            print $app->authorHtml();
        }
        if ($app->hasVersions()) {
            ///TODO: Versions not yet implemented
        }
        if ($app->hasLicense()) {
            print '<h2>License</h2>';
            print $app->licenseHtml();
        }
    }
}

function printDevelopmentPage($app, $category)
{
    print '<h2>Source Code Repository</h2>';

    if ($category == 'unmaintained') {
        print "<p><b>This app is unmaintained and no longer released by the KDE community.</b></p>";
        return;
    }

    print $app->browseSourcesHtml();

    print $app->checkoutSourcesHtml();

    //Show bugzilla related links only for applications hosted at bugs.kde.org
    if ($app->hasBugTracker() && !$app->isBugTrackerExternal()) {
        print '<h2>Search for open bugs</h2>';

        $product = $app->bugzillaProduct();

        $componentstring = "";
        if ($app->bugzillaComponent()) {
            $componentstring = '&component='.$app->bugzillaComponent();
        }

        $majorBugs  = 'https://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&product='.$product.$componentstring.'&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash';
        $minorBugs  = 'https://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&product='.$product.$componentstring.'&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=normal&bug_severity=minor';
        $wishes     = 'https://bugs.kde.org/buglist.cgi?short_desc_type=allwordssubstr&product='.$product.$componentstring.'&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=wishlist';
        $juniorJobs = 'https://bugs.kde.org/buglist.cgi?keywords=junior-jobs&product='.$product.$componentstring.'&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&cmdtype=doit';

        print '<ul>';
        print '<li><a href="'.htmlspecialchars($majorBugs).'">Major Bug reports</a></li>';
        print '<li><a href="'.htmlspecialchars($minorBugs).'">Minor Bug reports</a></li>';
        print '<li><a href="'.htmlspecialchars($wishes).'">Wish reports</a></li>';
        print '<li><a href="'.htmlspecialchars($juniorJobs).'">Junior Jobs</a></li>';
        print '</ul>';
    }

    if ($app->hasEbn()) {
        print '<h2>Code checking</h2>';
        print '<p>Show results of automated code checking on the English Breakfast Network (EBN).</p>';
        print '<ul>';
        print '<li><a href="'.htmlspecialchars($app->ebnCodeCheckingUrl()).'">Code Checking</a></li>';
        print '<li><a href="'.htmlspecialchars($app->ebnDocCheckingUrl()).'">Documentation Sanitation</a></li>';
        print '</ul>';
    }

}

require('config.php');
require(KDE_ORG . '/aether/config.php');

$pageConfig = array_merge($pageConfig, [
    'title' => $page_title,
    'cssFile' => 'https://cdn.kde.org/aether-devel/applications.css',
    'image' => '/' . $url_dir . '/icons/' . $app->icon(),
]);

require(KDE_ORG . '/aether/header.php');
$site_root = "../";
?>

<script src="/js/use-ekko-lightbox.js" defer="true"></script>
<main itemscope itemtype="http://schema.org/SoftwareApplication">
    <meta itemprop="operatingSystem" content="GNU/Linux" />
    <meta itemprop="name" content="<?php echo $app->name(); ?>" />
    <meta itemprop="isFamilyFriendly" content="true" />
    <meta itemprop="applicationCategory" content="<?php echo $app->category(); ?>" />
    <link href="/applications/fonts/css/fontawesome-all.css" rel="stylesheet">

    <div class="application-header pt-2 pb-2">
        <div class="container d-flex">
            <img src="/<?= $url_dir . '/icons/' . $app->icon() ?>" alt="<?= $app->name() ?> Icon" width="64" height="64" class="mr-2" />
            <div >
                <h1>
                  <a href="/<?= $url_dir ?>">KDE's Applications</a>
                  <?= $app->name() ?>
                </h1>
                <a href="/applications/<?= $category ?>"><?= $category ?></a>
            </div>
            <?php if ($category != 'unmaintained') { ?>
            <a class="noblefir ml-auto align-self-center" href="appstream://<?= $app->AppStreamId() ?>">
                <i class="fa fa-download"></i>
                Install
            </a>
            <?php } ?>
        </div>
    </div>

    <div class="container">
        <!-- <a href="/applications" class="mt-3 mb-3">&lt; Go back to the applications list</a> -->

        <?php if (!$development) {
            echo '<div class="mt-2">';
            echo $app->displayScreenshots();
            echo '</div>';
        } ?>

        <div class="row border-top">
            <div class="col-12 col-md-9 mb-3">
    <?php
                if (!$development) {
                    printPage($app, $category);
                } else {
                    printDevelopmentPage($app, $category);
                }
    ?>
            </div>
            <div class="col-12 col-md-3 mb-3 sidebar">
                <?php echo printSidebar($app, $category, $development); ?>
            </div>
        </div>
    </div>
</main>
<?php
require(KDE_ORG . '/aether/footer.php');
