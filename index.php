<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require('config.php');
require(KDE_ORG . '/aether/config.php');

$url_split = explode('/', $_SERVER['REQUEST_URI']);
$url_dir = $url_split[1];

$pageConfig = array_merge($pageConfig, [
    'title' => "KDE's Applications",
    'cssFile' => 'https://cdn.kde.org/aether-devel/applications.css',
    'image' => '/applications/apps.png',
]);

require(KDE_ORG . '/aether/header.php');
require('includes/AppDataBuilder.php');

$site_root = "../";

echo '<main class="container">';

echo '<h1><a href="/'.$url_dir.'/">KDE\'s Applications</a></h1>';

$index = json_decode(file_get_contents("index.json"), true);

$categories = array_keys($index);
sort($categories);
?>

<p>KDE is a community of friendly people who create over 200 apps which run on any Linux desktop, and often other platforms too. Here is the complete list.</p>

<div class="form-group">
    <label for="search" class="sr-only">Search</label>
    <input type="email" class="form-control" id="search" placeholder="Search...">
</div>

<?php

$appDataBuilder = new AppDataBuilder();

foreach($categories as $category) { ?>
    <div class="category"> <?php

    // category header
    $icon = "icons/categories/" . strtolower($category);
    // special case unmaintained to an unmaintained icon
    if ($category == 'unmaintained') {
        $icon = "icons/org.kde.blogilo"; ?>
        <p id="app-additional-info-paragraph"></p>
        <div id="app-additional-info">
        <?php
    } ?>
    <h2 style='font-size: x-large; font-weight: bold;' class='mt-5 mb-1'>
        <a href="<?= strtolower($category) ?>" class="ml-4 d-flex align-items-center">
            <img class="ml-1 mr-2" width="48" height="48" src="<?= $icon ?>.svg" alt="<?= $category ?>" icons" title="<?= $category ?>" />
            <?= ucfirst($category) ?>
        </a>
    </h2>

    <?php
    $array = $appDataBuilder->loadCategory($index, $category);
    echo '<h2>Applications</h2>';
    echo '<div class="row">';

    // all the apps in the category
    foreach($array['apps'] as $app) {
        $appDataBuilder->displayAppCard($app, $url_dir, $category);
    }
    echo '</div>';

    if (sizeof($array['addons']) > 0) {
        echo '<h2>Addons</h2>';
        echo '<div class="row">';
    }

    foreach($array['addons'] as $addon) {
        $appDataBuilder->displayAppCard($addon, $url_dir, $category);
    }
    if (sizeof($array['addons']) > 0) {
        echo '</div>';
    }

    echo '</div>';
}


function nameToUrl($s)
{
    return str_replace(' ', '', strtolower($s));
}


// Like str_replace but only replacing first occurance.
function str_replace_first($from, $to, $subject)
{
    $from = '/'.preg_quote($from, '/').'/';

    return preg_replace($from, $to, $subject, 1);
}

function addUrlToName($text, $appName, $url)
{
    return str_replace_first($appName, '<a href="'.$url.'"><strong>'.$appName.'</strong></a>', $text);
}

/*
$randomNumber = mt_rand(0, count($apps) - 1);
$app = new AppData2($apps[$randomNumber]);

$appurl = nameToUrl($app->category()).'/'.nameToUrl($app->id());

echo '<h2>Application Spotlight: '.addUrlToName($app->name(), $app->name(), $appurl).'</h2>';
print addUrlToName($app->descriptionHtml(), $app->name(), $appurl);

// FIXME: this is fairly duped from applicationpage2 (was before I got to it)
$thumbUrl = $app->defaultScreenshotThumbnailUrl();
$screenshotUrl = $app->defaultScreenshotUrl();
if ($screenshotUrl) {
    print '<div class="app-screenshot"><a href="'.$screenshotUrl.'">
    <img src="'.$thumbUrl.'" width=540 alt="Screenshot" />
    </a></div>'; ///TODO: image size
} else {
    print '<div class="app-screenshot">
    <img src="/images/screenshots/no_screenshot_available.png" alt="No screenshot available" />
    </div>'; ///TODO: image size
}
 */
?>

<script src="toggle.js" defer></script>
<p style="float: right"><a style="color: grey;" href="https://community.kde.org/KDE.org/applications">Page Setup Info</a></p>.
</main>
<script src="/applications/search.js" defer></script>
<?php
require(KDE_ORG . '/aether/footer.php');
