const search = document.getElementById('search');
search.addEventListener('input', function(e) {
    const apps = document.querySelectorAll('.app');
    const regex = new RegExp(e.target.value, 'i');
    apps.forEach(function(app) {
        if (app.querySelector('img').title.search(regex) !== -1) {
            // app name is in search
            if (app.classList.contains('d-none')) {
                app.classList.remove('d-none');
            }
        } else if (!app.classList.contains('d-none')) {
            app.classList.add('d-none');
        }
    });

    const categories = document.querySelectorAll('.category');
    categories.forEach(function(category) {
        if (category.querySelectorAll('.app:not(.d-none)').length > 0 && category.classList.contains('d-none')) {
            category.classList.remove('d-none');
        } else if (category.querySelectorAll('.app:not(.d-none)').length === 0 && !category.classList.contains('d-none')) {
            category.classList.add('d-none');
        }
    });
});
