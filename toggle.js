$(document).ready(function(){
    $('#app-additional-info').toggle();
    $('#app-additional-info-paragraph').append('<a href="#" id="app-additional-info-toggle">Show Unmaintained Applications</a>');
    //Add toggle effect
    $('#app-additional-info-toggle').click(function(){
        $('#app-additional-info').slideToggle('slow');
        return false;
    });
});
