<?php
/**
 * kde-org-applications
 * Copyright (C) 2019 Carl Schwan <carl at carlschwan dot eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Intl
{
    private $clientPreferredLanguage;

    public function __construct($acceptedLanguages = false)
    {
        if (empty($acceptedLanguages)) {
            $acceptedLanguages = $_SERVER["HTTP_ACCEPT_LANGUAGE"];
        }
        // print_r($acceptedLanguages); print_r("<br/>");

        // regex inspired from @GabrielAnderson on http://stackoverflow.com/questions/6038236/http-accept-language
        preg_match_all('/([a-z]{1,8}(-[a-z]{1,8})*)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i', $acceptedLanguages, $lang_parse);
        $langs = $lang_parse[1];
        $ranks = $lang_parse[4];

        // (create an associative array 'language' => 'preference')
        $lang2pref = array();
        for($i=0; $i<count($langs); $i++) {
            $lang2pref[$langs[$i]] = (float) (!empty($ranks[$i]) ? $ranks[$i] : 1);
        }

        // (comparison function for uksort)
        $cmpLangs = function ($a, $b) use ($lang2pref) {
            if ($lang2pref[$a] > $lang2pref[$b])
                return -1;
            elseif ($lang2pref[$a] < $lang2pref[$b])
                return 1;
            elseif (strlen($a) > strlen($b))
                return -1;
            elseif (strlen($a) < strlen($b))
                return 1;
            else
                return 0;
        };

        // sort the languages by prefered language and by the most specific region
        uksort($lang2pref, $cmpLangs);

        $this->clientPreferredLanguages = array_keys($lang2pref);
    }

    public function getPreferredLanguage()
    {
        return $this->clientPreferredLanguages[0];
    }

    public function getPreferredLanguages()
    {
        return $this->clientPreferredLanguages;
    }

    public function l10n($ary)
    {
        if (!is_array($ary)) {
            // No translations, nothing to do
            return $ary;
        }
        if (sizeof($ary) == 0) {
           //print "Empty l10n array";
           return;
        }
        static $__c_alternates = array('en', 'en-US');
        $ret = $ary['C'];
        foreach($this->getPreferredLanguages() as $lang) {
            // print_r($lang); print_r("<br/>");
            if (in_array($lang, $__c_alternates, true)) {
                return $ret; // Defaults to C already.
            }
            if (array_key_exists($lang, $ary)) {
                return $ary[$lang];
            }
        }
        return $ret;
    }
}
