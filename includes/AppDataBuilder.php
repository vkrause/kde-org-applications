<?php
/**
 * kde-org-applications
 * Copyright (C) 2019 Carl Schwan <carl at carlschwan dot eu>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require('./includes/Intl.php');
require('./includes/AppData.php');

/**
 * Class AppDataBuilder
 *
 * Create AppData class
 */
class AppDataBuilder
{
    private $prefix;
    private $intl;

    public function __construct($prefix = ".")
    {
        $this->prefix = $prefix;
        $this->intl = new Intl();
    }

    function load($name)
    {
        $name = preg_replace('/[^a-z\-_0-9\.]/i', '', $name);
        $filePath = $this->prefix . '/appdata/' . $name . '.json';

        // Make sure to resolve symlinks to their actual name. Specifically
        // symlinks may be used for backwards compatibility of old names, but
        // data resolution should happen on the current name.
        set_error_handler(function() { /* ignore errors */ });
        $link = readlink($filePath);
        restore_error_handler();

        if ($link !== false) {
            $filePath = $link;
            $name = basename($filePath, '.json');
        }

        if (!file_exists($filePath)) {
            http_response_code(404);
            return false;
        }

        $data = json_decode(file_get_contents($filePath), true);

        $extensionFile = $this->prefix . '/appdata-extensions/' . $name. '.json';
        if (file_exists($extensionFile)) {
            $data = array_merge_recursive($data, json_decode(file_get_contents($extensionFile), true));
        }

        if ($data['Type'] === 'desktop-application') {
            return new AppData($name, $data, AppDataType::Application, $this->intl);
        } else if ($data['Type'] === 'addon') {
            return new AppData($name, $data, AppDataType::Addon, $this->intl);
        } else if ($data['Type'] === 'console-application') {
            return new AppData($name, $data, AppDataType::Console, $this->intl);
        }
        return null;
    }

    /**
     * Load apps and addons separatly for a category
     */
    function loadCategory($index, $category)
    {
        $apps = [];
        $addons = [];

        foreach($index[$category] as $application) {
            $appData = $this->load($application);
            if ($appData->getType() === AppDataType::Application) {
                $apps[] = $appData;
            } else if ($appData->getType() === AppDataType::Console) {
                $apps[] = $appData;
            } else if ($appData->getType() === AppDataType::Addon) {
                $addons[] = $appData;
            }
        }

        return [
            'apps' => $apps,
            'addons' => $addons,
        ];
    }

    public function displayAppCard(/* AppData */ $app, $url_dir, $category)
    { 
?>
<div class="app mt-4 col-12 col-sm-6 col-md-4 col-lg-3">
    <div class="d-flex">
        <a href="<?= strtolower($category) . '/' . $app->getId() ?>">
            <img class="mr-2" width="48" height="48" src="/<?= $url_dir ?>/icons/<?= $app->icon() ?>" alt="<?= $app->name() ?>" title="<?= $app->name() ?>" />
        </a>
        <div>
            <a href="<?= strtolower($category) . '/' . $app->getId() ?>">
                <?= $app->name() ?>
            </a>
            <p><?= $app->genericName() ?></p>
        </div>
    </div>
</div>
        <?php
    }
}
