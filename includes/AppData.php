<?php

/**
* Get an array containing information for an application page on kde.org and gives
* access to its data.
* Written by Daniel Laidig <d.laidig@gmx.de>
* Rebased on new backend data by Harald Sitter <sitter@kde.org>
*
* Usage:
* <?php
*  TODO
* ?>
*/

/**
 * Enum IconType
 */
abstract class IconType {
    const Stock = 0;
    const Local = 1;
    const None = 2;
}

/**
 * Enum AppDataType
 */
abstract class AppDataType {
    const Application = 0;
    const Addon = 1;
    const Console = 2;
}

class AppData
{
    private $id;
    private $data;

    /**
     * @var AppDataType
     */
    private $type;

    /**
     * @var Intl
     */
    private $intl;

    public function __construct($id, $data, $type, $intl)
    {
        $this->id = $id;
        $this->data = $data;
        $this->type = $type;
        $this->intl = $intl;
    }

    public function getType()
    {
        return $this->type;
    }

    /**
     * Same as AppStreamId but with .desktop stripped to force consistent naming.
     * @return string
     */
    public function getId()
    {
        if (empty($this->id)) {
            return $this->data['X-KDE-ID'];
        }
        return $this->id;
    }

    public function name()
    {
        return $this->intl->l10n($this->data['Name']);
    }

    function genericName()
    {
        // FIXME: summary is a bad fit for this as it can be rather long
        // return $this->intl->l10n($this->data['Summary']);
        return $this->intl->l10n($this->data['X-KDE-GenericName']);
    }

    // FIXME: in appdata things can be in more than one category, the sturcture of the site is kinda only expecting things once
    function category()
    {
        return $this->data['Categories'][0];
    }

    function descriptionHtml() {
        $description = $this->intl->l10n($this->data['Description']);

        // Force paragraphing if the Description doesn't have it.
        if(substr($description, 0, 3) != '<p>') {
            $description = '<p>'.$description.'</p>';
        }

        return $description;
    }

    function hasVersions() {
        return false; ///TODO: version information is not yet implemented
    }

    function hasAuthors() {
        return isset($this->data['authors']);
    }

    function authorHtml() {
        $html = '<p><strong>'.i18n_var( "Authors:" ).'</strong></p>';
        $html .= '<ul>';
        foreach($this->data['authors'] as $author) {
            $html.= '<li itemprop="author" itemscope itemtype="http://schema.org/Person"><span>'.$author[0].'</span>';
            if (strlen($author[2])) {
                $html .= ' <a itemprop="email" href="mailto:'.htmlspecialchars($author[2]).'">&lt;'.htmlspecialchars($author[2]).'&gt;</a>';
            }
            $html.='<br /><span itemprop="name">'.i18n_var($author[1]).'</span>';
            if (strlen($author[3])) {
                $html .= '<br /><a itemprop="url" href="'.htmlspecialchars($author[3]).'">'.htmlspecialchars($author[3]).'</a>';
            }
            $html .= '</li>';
        }
        $html .= '</ul>';
        if (count($this->data['credits'])) {
            $html .= '<p><strong>'.i18n_var( "Thanks To:" ).'</strong></p>';
            $html .= '<ul>';
            foreach($this->data['credits'] as $author) {
                $html.= '<li><span>'.$author[0].'</span>';
                if (strlen($author[2])) {
                    $html .= ' <a href="mailto:'.htmlspecialchars($author[2]).'">&lt;'.htmlspecialchars($author[2]).'&gt;</a>';
                }
                $html.='<br />'.i18n_var($author[1]);
                if (strlen($author[3])) {
                    $html .= '<br /><a href="'.htmlspecialchars($author[3]).'">'.htmlspecialchars($author[3]).'</a>';
                }
                $html .= '</li>';
            }
            $html .= '</ul>';
        }
        return $html;
    }

    function hasLicense() {
        return isset($this->data['ProjectLicense']);
    }

    function licenseHtml() {
        // TODO: we should really render SPDX properly. There are some databases
        //   of licenses and their texts. Or we could send URLs to spdx.org.
        $license = $this->data['ProjectLicense'];
        $text = '<p>';
        // $stripped_license = str_replace("+", "", $license);
        // $text = i18n_var('%1 is distributed under the terms of the <script type="text/javascript">document.write(spdxToHTML("%3"))</script></a>.',
        //                  $this->name(), $stripped_license, $license);
        switch($license) {
        case 'GPL-2.0+': case 'GPL-2.0': case 'GPL-2.0-or-later': case 'GPL-2.0-only':
            $text .= i18n_var('%1 is distributed under the terms of the <a itemprop"license" href="http://www.gnu.org/licenses/old-licenses/gpl-2.0.html">GNU General Public License (GPL), Version 2</a>.', $this->name());
            break;
        case 'GPL-3.0+': case 'GPL-3.0': case 'GPL-3.0-or-later': case 'GPL-3.0-only':
            $text .= i18n_var('%1 is distributed under the terms of the <a itemprop="license" href="http://www.gnu.org/licenses/gpl.html">GNU General Public License (GPL), Version 3</a>.', $this->name());
            break;
        case 'LGPL-2.0-only': case 'LGPL-2.0-or-later': case 'LGPL-2.1-only': case 'LGPL-2.1-or-later': case 'LGPL':
            $text .= i18n_var('%1 is distributed under the terms of the <a itemprop="license" href="http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html"> GNU Lesser General Public License, version 2</a>.', $this->name());
            break;
        case 'LGPL-3.0-only': case 'LGPL-3.0-or-later': case 'LGPL-3.0':
            $text .= i18n_var('%1 is distributed under the terms of the <a itemprop="license" href="https://www.gnu.org/licenses/lgpl.html"> GNU Library General Public License, version 3</a>.', $this->name());
            break;
        default:
            $text .= $license.'.';
        }
        $text .= '</p>';
        return $text;
    }

    function hasHomepage() {
        return isset($this->data['Url']['homepage']);
    }

    function homepage() {
        return $this->data['Url']['homepage'];
    }

    function hasAppStream() {
        return true;
    }

    function AppStreamId() {
        return $this->data['ID'];
    }

    function getIconType() {
        if (isset($this->data['Icon']['local'])) {
            return IconType::Local;
        } else if (isset($this->data['Icon']['stock'])) {
            return IconType::Stock;
        }
        return IconType::None;
    }

    function hasIcon() {
        return $this->getIconType() !== IconType::None;
    }

    function icon() {
        $iconType = $this->getIconType();

        switch ($iconType) {
        case IconType::Local:
            return $this->data['Icon']['local'][0]['name'];
            break;

        case IconType::Stock:
            $iconName = $this->data['Icon']['stock'];

            // TODO find icon path

            break;

        case IconType::None:
            // TODO
            break;

        default:
            throw new \Exception("Should not happen, IconType is undefined");
        }
    }

// TODO: not in appdata
    function hasKDEApps() {
        return isset($this->data['kde-apps.org']);
    }
// FIXME: not in appdata
    function KDEAppsId() {
        return $this->data['kde-apps.org'];
    }
// FIXME: not in appdata
    function hasUserbase() {
        return isset($this->data['userbase']);
    }
// FIXME: not in appdata
    function userbase() {
        return "http://userbase.kde.org/".$this->data['userbase'];
    }

    function hasHandbook() {
        return isset($this->data['Url']['help']);
    }
    function handbook() {
        return $this->data['Url']['help'];
    }

    // AppData extension X-KDE-Forum: <String>
    function forumUrl() {
        if (isset($this->data['X-KDE-Forum'])) {
            return 'http://forum.kde.org/viewforum.php?f='.$this->data['X-KDE-Forum'];
        } else {
            return 'http://forum.kde.org/';
        }
    }

    // AppData extension X-KDE-IRC: <String, Array>
    function ircChannels() {
        if (isset($this->data['X-KDE-IRC'])) {
            $irc = $this->data['X-KDE-IRC'];
            if (is_array($irc)) {
                return $irc;
            } else {
                return array($irc);
            }
        } else {
            return array("#kde");
        }
    }

    // AppData extension X-KDE-MailingLists: <String, Array>
    function mailingLists() {
        if (isset($this->data['X-KDE-MailingLists'])) {
            $ml = $this->data['X-KDE-MailingLists'];
            if (is_array($ml)) {
                return $ml;
            } else {
                return array($ml);
            }
        } else {
            return array("kde@kde.org");
        }
    }

    // AppData extension X-KDE-Repository: <String>
    function repository() {
        return $this->data['X-KDE-Repository'];
    }

    function browseSourcesHtml() {
        $path = '<p><a href="https://cgit.kde.org/'.$this->repository() . '.git';
        $path .= '">'.i18n_var("Browse %1 source code online", $this->name()).'</a></p>';
        return $path;
    }

    function checkoutSourcesHtml() {
        return '<p>'.i18n_var("Clone %1 source code:",
                              $this->name()).'</p><p><code>git clone https://anongit.kde.org/'.$this->repository().'</code></p>';
    }

    //Returns true if the application has a bugtracker (external or KDE bugzilla)
    function hasBugTracker() {
        return isset($this->data['Url']['bugtracker']);
    }

    function bugTracker() {
        return $this->data['Url']['bugtracker'];
    }

// FIXME: not implemented url types in frontend:
// faq
// donation
// translate

    // TODO: appstream bts are always complete URLs so this code is pointless
    // Returns true if the application has an external bugtracker (!bugs.k.o)
    function isBugTrackerExternal() {
        return (substr($this->bugTracker(), 0, 7) == "http://" ||
        substr($this->bugTracker(), 0, 8) == "https://");
    }

    function bugzillaProduct() {
        if (is_array($this->bugTracker())) {
            return $this->bugTracker()[0];
        }
        return $this->bugTracker();
    }

    function bugzillaComponent() {
        if (is_array($this->bugTracker())) {
            return $this->bugTracker()[1];
        }
        return false;
    }

    // TODO: ebn is broken for now
    function hasEbn() {
        return false;
    }

    function ebnUrlBase() {
        // examples:
        // KDE/kdeedu/parley:               http://englishbreakfastnetwork.org/krazy/reports/kde-4.x/kdeedu/parley/index.html
        // KDE/kdebase/apps/dolphin:        http://englishbreakfastnetwork.org/krazy/reports/kde-4.x/kdebase-apps/dolphin/index.html
        // extragear/graphics/digikam:      http://englishbreakfastnetwork.org/krazy/reports/extragear/graphics/digikam/index.html
        // koffice/kword:                   http://englishbreakfastnetwork.org/krazy/reports/bundled-apps/koffice/kword/index.html
        // amarok (gitorious):              http://englishbreakfastnetwork.org/krazy/reports/extragear/multimedia/amarok-git/index.html

        $ebn = '';
        // extract path segments from module parent
        $parts = explode('/', $this->data['parent']);

        // replace path segment by "kde-4.x" for "kde"
        if ($parts[0] == 'kde') {
            $ebn = "kde-4.x/" . $parts[1] . "/" . strtolower($this->data['repository'][1]);
        } else {
            $ebn = $parts[0] . "/" . $parts[1] . "/" . strtolower($this->name());
        }
        return $ebn;
    }

    function ebnCodeCheckingUrl()
    {
        return 'http://ebn.kde.org/krazy/reports/'.$this->ebnUrlBase().'/index.html';
    }

    function ebnDocCheckingUrl()
    {
        return 'http://ebn.kde.org/sanitizer/reports/'.$this->ebnUrlBase().'/index.html';
    }

    /**
     * Get the screenshots.
     * @return array Return an array of screenshots, if no screenshot is present return an empty array.
     *               Each items contains a thumbnails (optional), a caption (optional) and a source-image.
     * @see https://www.freedesktop.org/software/appstream/docs/chap-Metadata.html#tag-screenshots
     */
    function getScreenshots() {
        // TODO: implement l10n
        $screenshots = [];

        if (!array_key_exists('Screenshots', $this->data)) {
            return $screenshots;
        }

        $lang = 'C';

        foreach($this->data['Screenshots'] as $screenshot) {
            if ($screenshot['source-image']['lang'] === $lang) {

                $screenshotRet = [
                    'thumbnails' => '../'.$screenshot['thumbnails'][0]['url'],
                    'source-image' => $screenshot['source-image']['url'],
                    'default' => isset($screenshot['default']) && $screenshot['default'] == true
                ];
                if (isset($screenshot['caption'])) {
                    $screenshotRet['caption'] = $this->intl->l10n($screenshot['caption']);
                }

                $screenshots[] = $screenshotRet;
            }
        }

        return $screenshots;
    }

    function displayScreenshots() {
        //Print screenshot or dummy "no screenshot available" image
        $screenshots = $this->getScreenshots();
        if (sizeof($screenshots) === 0) {
            // no screenshot, display dummy "no screenshot available" image
        ?>
        <div class="d-flex justify-content-center mb-2">
            <img class="img-fluid" src="/images/screenshots/no_screenshot_available.png" alt="No screenshot available" />
        </div>
        <?php
        } else if (sizeof($screenshots) === 1) {
            $screenshot = $screenshots[0];
        ?>
            <div class="d-flex justify-content-center mb-2">
                <a href="<?php echo($screenshot['source-image']); ?>" data-toggle="lightbox">
                    <img class="img-fluid" src="<?php echo($screenshot['thumbnails']); ?>" alt="Screenshot <?php echo($this->name()); ?>" />
                </a>
            </div>
        <?php
        } else {
        ?>
        <div id="carouselAppControls" class="carousel slide mb-2" data-ride="carousel">
            <div class="carousel-inner">
            <?php
            foreach ($screenshots as $index => $screenshot) { ?>
                <div class='carousel-item <?php echo($index === 0 ? 'active' : ''); ?>'>
                     <a href="<?php echo($screenshot['source-image']); ?>" data-toggle="lightbox">
                        <img src='<?php echo $screenshot['source-image'] ?>' class='img-fluid' alt='<?php (isset($screenshot['caption']) ? $screenshot['caption'] : ($this->name() . ' screenshot')); ?>'>
                    </a>
                </div>
            <?php
            }
    ?>
            </div>
            <a class="carousel-control-prev" href="#carouselAppControls" role="button" data-slide="prev" onclick="return false;">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselAppControls" role="button" data-slide="next" onclick="return false;">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    <?php
        }
    }
}
